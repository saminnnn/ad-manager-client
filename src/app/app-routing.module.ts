import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { AdProjectsComponent } from './ad-projects/ad-projects.component';
import { HomeComponent } from './home/home.component';
import { ViewProjectComponent } from './ad-projects/view-project/view-project.component';

const routes: Routes = [
  {path: 'adprojects', component: AdProjectsComponent},
  {path: '', component: HomeComponent},
  {path: 'adprojects/:projectname', component: ViewProjectComponent}
]

@NgModule({
  imports: [RouterModule.forRoot(routes)  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
