import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiServiceService, CommonResponse } from '../api-service.service';

@Component({
  selector: 'app-ad-projects',
  templateUrl: './ad-projects.component.html',
  styleUrls: ['./ad-projects.component.css']
})
export class AdProjectsComponent implements OnInit {
  projects: string[];
  newProjectName: string;
  responseMessage: string;
  success: boolean;
  projectMessages: string[];
  showFileRepalceAlert =  false;
  showFileDeleteAlert = false;
  pendingIndex: number;
  pendingForm: FormData;
  pendingToken: string;
  pendingProject: string;
  

  constructor(private api: ApiServiceService) {   }

  ngOnInit(): void {
    this.getProjects();
  }

  getProjects() {
    this.api.getData('filewrite/projects').subscribe((res: string[]) => {
      this.projects = res;
      this.projectMessages = new Array(res.length);
    });
  }

  addProject() {
    this.api.postData('filewrite/newProject', {name: this.newProjectName}).subscribe((res: CommonResponse) => {
      this.success = res.success;
      this.responseMessage = this.newProjectName + ' ' + res.message;
      if(res.success) {
        this.newProjectName = '';
        this.getProjects();
      }
    });
  }

  deleteProject(name: string, index: number, fullProject: boolean) {
      this.sendDeleteRequest(name, fullProject).subscribe((res: CommonResponse) => {
      if(fullProject) {
        this.success = res.success;
        this.responseMessage = res.message;
        this.projects.splice(index, 1);
      } else {
        this.projectMessages[index] = 'All files deleted';
      }
      
    })
  }

  sendDeleteRequest(name: string, fullProject: boolean): Observable<CommonResponse> {
    return this.api.postData('filewrite/deleteProject', {name, fullProject});
  }

  fileSelected(event: Event, projectName: string, index: number) {
    const formData = new FormData();
    const file: File = ((event.target as HTMLInputElement).files as FileList).item(0) as File;
    if(!file) return;
    formData.append('file', file);

    this.api.postData('filewrite/checkProject', {name: projectName}).subscribe((status: CheckProject) => {
      if(status.hasFiles) {
        this.showFileRepalceAlert = true;
        this.pendingIndex = index;
        this.pendingProject = projectName;
        this.pendingToken = status.token;
        this.pendingForm = formData;
      } else {
        this.uploadFile(status.token, formData, index);
      }
    });
  }
 
  alertRespone(yes: boolean) {
    this.showFileRepalceAlert = false;
    if(yes) {
      this.sendDeleteRequest(this.pendingProject, false).subscribe((res: CommonResponse) => {
        if(!res.success) return;
        this.uploadFile(this.pendingToken, this.pendingForm, this.pendingIndex)
        
      })
    }
  }

  uploadFile(token: string, formDate: FormData, index: number) {
    const link = 'filewrite/uploadzip/' + token;
    this.api.postFileAsForm(link, formDate).subscribe((res: CommonResponse) => {
      this.projectMessages[index] = res.message;
      this.removePendingObject();
    })
  }

  readMnaifest(event: Event, project: string) {
    const file: File = ((event.target as HTMLInputElement).files as FileList).item(0) as File;
    if(!file) return;
    const fileReader = new FileReader();
    fileReader.readAsText(file);

    fileReader.onload = () => {
      const manifest: string = fileReader.result as string;
      
      this.api.postData('filewrite/submitManifest', {project, manifest}).subscribe((res: CommonResponse) => {
        this.projectMessages[this.pendingIndex] = res.message;
        this.removePendingObject();
      });
    }
  }

  fileInputClick(id: string, index: number) {
    console.log(index)
    this.pendingIndex = index;
    document.getElementById(id)?.click();
  }

  deleteClicked(project: string, index: number) {
    this.showFileDeleteAlert = true;
    this.pendingProject = project;
    this.pendingIndex = index;
  }

  deleteResponse(yes: boolean) {
    this.showFileDeleteAlert = false;
    if(yes) {
      this.deleteProject(this.pendingProject, this.pendingIndex, true);
    }
    this.removePendingObject();
  }


  removePendingObject() {
    this.pendingIndex = -1;
    this.pendingToken = '';
    this.pendingProject = '';
    this.pendingForm = new FormData();
  }
}

interface CheckProject {project: string, token: string, hasFiles: boolean}