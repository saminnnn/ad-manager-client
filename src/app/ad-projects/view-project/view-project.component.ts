import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { ApiServiceService } from 'src/app/api-service.service';

@Component({
  selector: 'app-view-project',
  templateUrl: './view-project.component.html',
  styleUrls: ['./view-project.component.css']
})
export class ViewProjectComponent implements OnInit {
  projectName: string;
  adLink: SafeResourceUrl;

  constructor(private route: ActivatedRoute, private sanitizer: DomSanitizer, private api: ApiServiceService) {
    this.projectName = this.route.snapshot.paramMap.get('projectname') as string;
  }

  ngOnInit(): void {
    const link = this.api.baseLink + 'filewrite/serveAd/' + this.projectName;
    this.adLink = this.sanitizer.bypassSecurityTrustResourceUrl(link);
  }

}
